#!/bin/sh

grep_url="https://dl.google.com/dl/android/studio/ide-zips/[0-9.]*/android-studio-ide-[0-9.]*-linux.tar.gz"

android_studio_url=$(curl https://developer.android.com/studio/ 2> /dev/null \
  | grep --max-count=1 -io "$grep_url")

curl "$android_studio_url" -O
tar xf android-studio-ide-*-linux.tar.gz
