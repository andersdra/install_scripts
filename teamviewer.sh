#!/bin/sh
# installs the latest teamviewer for Debian/Ubuntu

curl --location 'https://download.teamviewer.com/download/linux/teamviewer_amd64.deb' -O

dpkg -i teamviewer_amd64.deb
apt-get install --fix-broken --yes
