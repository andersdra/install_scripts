#!/bin/sh
# build ungoogled-chromium for Debian Buster using docker
# modified version of repo build script
# https://github.com/ungoogled-software/ungoogled-chromium-debian.git
# USAGE:
: '
docker run --name ungoogled-chromium -i debian:buster < ungoogled_chromium_build.sh
mkdir ungoogled-chromium
docker cp ungoogled-chromium:/ungoogled-chromium ungoogled-chromium
'

apt-get update
apt-get install --yes git gpg python3 packaging-dev wget

mkdir -p build/src
git clone --recurse-submodules https://github.com/ungoogled-software/ungoogled-chromium-debian.git
git checkout --recurse-submodules debian_buster

cp -r ungoogled-chromium-debian/debian build/src/
cd build/src

./debian/rules setup-debian

echo 'deb http://apt.llvm.org/buster/ llvm-toolchain-buster-8 main' > /etc/apt/sources.list.d/llvm.list
echo 'deb-src http://apt.llvm.org/buster/ llvm-toolchain-buster-8 main' >> /etc/apt/sources.list.d/llvm.list
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

apt-get update

mk-build-deps --tool "apt-get -o Debug::pkgProblemResolver=yes --no-install-recommends --yes" -i debian/control
rm ungoogled-chromium-build-deps_*.deb

./debian/rules setup-local-src

dpkg-buildpackage -b -uc \
&& mkdir /ungoogled-chromium \
&& mv /build/*.deb /ungoogled-chromium \
&& exit 0
