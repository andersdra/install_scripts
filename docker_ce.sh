#!/bin/sh
# installs docker-ce on Debian/Ubuntu / Fedora via repositories
# adds SUDO_USER to docker group
# run as root

if [ ! "$USER" = 'root ' ] && [ -z "$SUDO_USER" ]
then
  if ! grep docker /proc/1/cgroup > /dev/null
  then
    echo 'must be run as root'
    exit 1
  fi
fi

if [ -n "$1" ] && [ "$1" = '-h' ] || [ "$1" = '--help' ]
then
  echo 'installs docker-ce from repositories on Debian/Ubuntu / Fedora'
  echo "if run with sudo, SUDO_USER will be added to 'docker' group"
  echo 'stable is default'
  echo './docker_install.sh {test,nightly} for alternative repos'
  exit 1
fi

if command -v apt-get > /dev/null
then
  apt-get update
  apt-get install --no-install-recommends --yes \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common

  distro_version='debian'
  release_version='stable'

  if [ "$1" ]
  then
    echo "Adding $1 repository"
    release_version="$1"
  fi

  if lsb_release -i | grep Ubuntu
  then
    distro_version='ubuntu'
  fi
  curl -fsSL "https://download.docker.com/linux/$distro_version/gpg" | apt-key add -
  apt-key fingerprint 0EBFCD88
  add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/$distro_version \
     $(lsb_release -cs) \
     $release_version"
  apt-get update
  apt-get install --yes docker-ce docker-compose
elif command -v dnf > /dev/null
then
  dnf -y install dnf-plugins-core
  dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
  if [ "$1" ]
  then
    echo "Activating repository $1 channel"
    dnf config-manager --set-enabled docker-ce-"$1"
  fi
  dnf -y install docker-ce docker-ce-cli docker-compose containerd.io || exit 69
fi

if [ "$SUDO_USER" ]
then
  echo "Adding '$SUDO_USER' to 'docker' group"
  usermod -aG docker "$SUDO_USER"
fi

if ! grep docker /proc/1/cgroup > /dev/null
then
  systemctl start docker
fi

if docker run hello-world
then
  exit 0
else
  exit 1
fi
