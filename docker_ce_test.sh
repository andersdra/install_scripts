#!/bin/sh
# automated verification of docker-ce install script

docker run --rm \
 -v /var/run/docker.sock:/var/run/docker.sock \
 -v "$PWD"/docker_ce.sh:/docker_ce.sh \
 fedora:latest \
 /bin/bash -c "/docker_ce.sh nightly" > fedora_log 2>&1 \
|| touch fedora_failed &

# let fedora finish or dnf mirror times out..
wait

docker run --rm \
 -v /var/run/docker.sock:/var/run/docker.sock \
 -v "$PWD"/docker_ce.sh:/docker_ce.sh \
 debian:testing \
 /docker_ce.sh > debian_log 2>&1 \
|| touch debian_failed &

docker run --rm \
 -v /var/run/docker.sock:/var/run/docker.sock \
 -v "$PWD"/docker_ce.sh:/docker_ce.sh \
 ubuntu:latest \
 /docker_ce.sh > ubuntu_log 2>&1 \
|| touch ubuntu_failed &

wait

if ls ./*_failed > /dev/null 2>&1
then
  find . -name "*_failed"
  find . -name "*_failed" -delete
  exit 1
else
  echo 'all passed'
  exit 0
fi
