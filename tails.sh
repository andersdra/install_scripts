#!/bin/sh
# download, verify and transfer tails iso to flash drive
# ./tails.sh /dev/sdc

dd_bs_setting='16MB'

if [ -z "$1" ]
then
  echo 'No drive input, will only download and verify'
  flash_drive=''
else
  echo "Will download, verify and transfer ISO to drive: $1 bs: $dd_bs_setting"
  flash_drive="$1"
fi

grep_page()
{
  curl "$1" 2> /dev/null | grep -io "$2"
}

tails_download_page='https://tails.boum.org/install/expert/usb/index.en.html'
tails_iso_grep="http://dl.amnesia.boum.org/tails/stable/tails-amd64-[0-9.]*/tails-amd64-[0-9.]*.img"
tails_sig_grep="https://tails.boum.org/torrents/files/tails-amd64-[0-9.]*.img.sig"

tails_iso_url=$(grep_page "$tails_download_page" "$tails_iso_grep")
tails_sig_url=$(grep_page "$tails_download_page" "$tails_sig_grep")

# download
curl --location "$tails_iso_url" > tails.img \
  || { printf '\n######\nFailed downloading ISO\n######\n' && exit 1; }
curl --location "$tails_sig_url" > tails.img.sig \
  || { printf '\n######\nFailed downloading signature\n######\n' && exit 1; }

# Verify ISO and transfer to flash drive
# get public key
TZ=UTC gpg --no-options --keyid-format long --verify tails.img.sig tails.img

printf '\n\n######\nCopy and paste RSA key, check all output carefully\n######\n'

read tails_public_key \
  && gpg --receive-keys "$tails_public_key"

# verify, transfer
if TZ=UTC gpg --no-options --keyid-format long --verify tails.img.sig tails.img
then
  if [ -z "$flash_drive" ]
  then
    printf '\nNo drive path input. Finished downloading and verifying\n'
    exit 1
  else
    echo "Starting transfer of ISO to drive: $flash_drive"
    dd if=tails.img of="$flash_drive" bs="$dd_bs_setting" status=progress \
    && sync \
    && echo 'Finished transferring ISO to drive' \
    && exit 0
    # exit if dd fails
    exit 1
  fi
fi
