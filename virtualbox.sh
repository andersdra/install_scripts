#!/bin/sh
# downloads and installs latest stable virtualbox release for latest Debian/Ubuntu / Fedora
# run as root

cleanup()
{
  find "$PWD" -maxdepth 1 -name "virtualbox-*_amd64.deb" -delete
  find "$PWD" -maxdepth 1 -name "VirtualBox-*.rpm" -delete
  find "$PWD" -maxdepth 1 -name "Oracle_VM_VirtualBox_Extension_Pack-*.vbox-extpack" -delete
  find "$PWD" -maxdepth 1 -name "SHA256SUMS.vbox" -delete
}

# install dependencies required for compiling kernel module

# debian
command -v apt-get > /dev/null 2>&1 \
&& apt-get update \
&& apt-get install --yes build-essential "linux-headers-$(uname -r)" \
&& vbox_binary_url=$(curl --silent --show-error 'https://www.virtualbox.org/wiki/Linux_Downloads' | \
  grep --max-count=1 -io 'https://download.virtualbox.org/virtualbox/[0-9.]*/[a-z0-9_.~-]*.deb')

# fedora
command -v dnf > /dev/null 2>&1 \
&& dnf -y install @development-tools \
&& dnf -y install kernel-headers kernel-devel kernel-devel-"$(uname -r)" dkms elfutils-libelf-devel qt5-qtx11extras \
&& vbox_binary_url=$(curl --silent --show-error 'https://www.virtualbox.org/wiki/Linux_Downloads' | \
  grep --max-count=1 -io 'https://download.virtualbox.org/virtualbox/[0-9.]*/[a-z0-9_.~-]*fedora[a-z0-9_.-]*.rpm' )

# make sure no older binaries are present in folder
cleanup

# extension pack url
vbox_extpack_url=$(curl --silent --show-error https://www.virtualbox.org/wiki/Downloads | \
  grep -io 'https://download.virtualbox.org/virtualbox/[0-9.]*/[a-z0-9._-]*.vbox-extpack')

curl --location "$vbox_binary_url" -O
curl --location "$vbox_extpack_url" -O

# get versions, may become handy on scripted updates
vbox_version=$(find . -name "virtualbox-*.deb" | cut -d '-' -f2 || find . -name "VirtualBox-*.rpm" | cut -d '-' -f2-3)
extpack_version=$(find . -name "Oracle_VM_VirtualBox_Extension_Pack-*.vbox-extpack" | cut -d '-' -f2)

# get sha256sums
vbox_base_url='https://www.virtualbox.org'
vbox_sha256_sums=$(curl --silent --show-error https://www.virtualbox.org/wiki/Linux_Downloads | \
  grep -io '/download/hashes/[0-9.]*/SHA256SUMS')
vbox_sha256_url="$vbox_base_url$vbox_sha256_sums"
curl --silent --show-error "$vbox_sha256_url" > SHA256SUMS.vbox

# verify and install
sha256sum --ignore-missing --check SHA256SUMS.vbox \
&& if command -v apt-get > /dev/null 2>&1;
then
  dpkg --install "$(find "$PWD" -name "virtualbox-*_amd64.deb")"
  apt-get install --fix-broken --yes
elif command -v dnf > /dev/null 2>&1
then
  dnf -y install "$(find "$PWD" -name "VirtualBox-*.rpm")"
  printf '#!/bin/bash\nexport QT_SCREEN_SCALE_FACTORS=1\nexport QT_SCALE_FACTOR=1\nexport QT_AUTO_SCREEN_SCALE_FACTOR=0\nexec virtualbox' \
    > virtualbox_start.bash
  chmod +x virtualbox_start.bash
fi \
&& echo 'y' | VBoxManage extpack install --replace "$(find "$PWD" -name "Oracle_VM_VirtualBox_Extension_Pack-*.vbox-extpack")"
cleanup
/sbin/vboxconfig
exit 0 \
|| echo "Verification failed!"; exit 1

