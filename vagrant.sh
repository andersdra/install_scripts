#!/bin/sh
# vagrant install

vagrant_url=$(curl https://www.vagrantup.com/downloads.html \
 | grep -io "https://releases.hashicorp.com/vagrant/[0-9.]*/vagrant_[0-9.]*_x86_64.deb")

curl --location "$vagrant_url" > vagrant_install.deb

dpkg --install vagrant_install.deb
apt-get install --fix-broken --yes
rm vagrant_install.deb
