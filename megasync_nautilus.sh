#!/bin/sh
# installs megasync and nautilus integration

if [ -z "$1" ]
then
  debian_version='10'
else
  debian_version="$1"
fi

curl \
  --location "https://mega.nz/linux/MEGAsync/Debian_$debian_version.0/amd64/megasync-Debian_$debian_version.0_amd64.deb" \
  > megasync_install.deb

curl \
  --location "https://mega.nz/linux/MEGAsync/Debian_$debian_version.0/amd64/nautilus-megasync-Debian_$debian_version.0_amd64.deb" \
  > megasync_nautilus.deb

sudo dpkg --install megasync_install.deb
sudo dpkg --install megasync_nautilus.deb
sudo apt-get install --fix-broken --yes

rm megasync_*.deb
