#!/bin/sh
# install with 'sudo -Es'

if [ -z "$1" ]
then
  tracked_release='stable'
else
  tracked_release="$1"
fi

# check if running in VirtualBox
if command -v dmidecode > /dev/null
then
  virtual_machine=$(if dmidecode | grep VirtualBox > /dev/null;then echo 0;else echo 1;fi)
else
  virtual_machine=$(if dd if=/dev/mem 2> /dev/null | strings | grep VirtualBox;then echo 0;else echo 1;fi)
fi

if [ "$virtual_machine" -lt 1 ]
then
  apt-get purge --yes libreoffice* rhythmbox*
  apt-get autoremove --yes
fi

if [ ! "$tracked_release" = 'stable' ]
then
  apt-get update
  apt-get upgrade --yes
  apt-get install --yes wget
  current_release=$(lsb_release -cs)
  # get codename of current testing release, safer than tracking testing
  testing_codename=$(wget -qO- http://ftp.debian.org/debian/dists/testing/Release \
    | grep Codename | cut -d ':' -f2 | xargs)
  # buster security entry does not match bullseye
  if [ "$current_release" = 'buster' ]
  then
    sed -i "s/buster\\/updates/$testing_codename-updates/g" /etc/apt/sources.list
  fi
  sed -i "s/$current_release/$testing_codename/g" /etc/apt/sources.list
  apt-get update
  apt-get upgrade --yes
  apt-get purge --yes unattended-upgrades
  apt-get autoremove --yes
  apt-get install --yes \
  apt-listbugs \
  apt-listchanges
fi

# Setup packages to be installed depending on if host or VM
if [ "$virtual_machine" -lt 1 ]
then
  apt_get_install="build-essential
  curl
  git
  guake
  linux-headers-$(uname -r)
  python3-pip
  tilix
  tmux
  unrar-free
  xclip"
else
  apt_get_install="build-essential
  curl
  fail2ban
  freeplane
  gettext
  git
  gnome-shell-pomodoro
  gparted
  gtkterm
  guake
  kicad
  linux-headers-$(uname -r)
  minicom
  nmap
  python3-pip
  rkhunter
  rsync
  smartmontools
  sqlite3
  sqlitebrowser
  sshfs
  tilix
  tlp
  tmux
  ufw
  unrar-free
  wireshark
  xclip"
fi

# apt-get
apt-get update
DEBIAN_FRONTEND=noninteractive \
apt-get install --yes $(echo "$apt_get_install" | sort)

# bash-it
git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it \
&& echo 'y' | ~/.bash_it/install.sh \
&& sed -i 's/bobby/clean/g' ~/.bashrc # change theme

# GNOME
gsettings set org.gnome.desktop.interface enable-animations false
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.interface clock-show-seconds true
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'

gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,close'
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button false
gsettings set org.gnome.desktop.wm.preferences action-middle-click-titlebar 'minimize'

gsettings set org.gnome.desktop.calendar show-weekdate true

# disable gnome keyring ssh agent
user_home="/home/$SUDO_USER"
autostart_folder="$user_home/.config/autostart"
/usr/bin/cp --recursive /etc/xdg/autostart/ $autostart_folder
/usr/bin/echo 'X-GNOME-Autostart-enabled=false' >> $autostart_folder/gnome-keyring-ssh.desktop
/usr/bin/chmod --recursive 644 $autostart_folder
/usr/bin/chgrp --recursive $SUDO_USER $autostart_folder
/usr/bin/chown --recursive $SUDO_USER $autostart_folder

# GSCONNECT
if [ "$virtual_machine" -eq 1 ]
then
  cd
  gsconnect_href=$(curl --location 'https://github.com/andyholmes/gnome-shell-extension-gsconnect/releases/latest' \
    | grep --max-count=1 -io '/andyholmes/gnome-shell-extension-gsconnect/releases/download/v[0-9.]*/gsconnect@andyholmes.github.io.zip') \
  && curl --location "https://github.com/$gsconnect_href" > gsconnect \
  && mkdir -p ~/.local/share/gnome-shell/extensions \
  && rm -rf ~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io \
  && unzip -o gsconnect -d ~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io
  gnome-shell-extension-tool -e gsconnect@andyholmes.github.io
  ufw allow 1714:1764/udp
  ufw allow 1714:1764/tcp
  ufw reload
fi

# CAFFEINE
if [ "$virtual_machine" -eq 1 ]
then
  cd
  git clone git://github.com/eonpatapon/gnome-shell-extension-caffeine.git
  cd gnome-shell-extension-caffeine
  ./update-locale.sh
  glib-compile-schemas --strict --targetdir=caffeine@patapon.info/schemas/ caffeine@patapon.info/schemas
  cp -r caffeine@patapon.info ~/.local/share/gnome-shell/extensions
  gnome-shell-extension-tool -e caffeine@patapon.info
  cd
  rm -rf gnome-shell-extension-caffeine
fi

# NO TITLE BAR
git clone https://github.com/franglais125/no-title-bar.git
cd no-title-bar
make install
gnome-shell-extension-tool -e no-title-bar@franglais125.gmail.com
cd
rm -rf no-title-bar

# /GNOME

# SPOTIFY
if [ "$virtual_machine" -eq 1 ]
then
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys A87FF9DF48BF1C90
  echo deb http://repository.spotify.com stable non-free | tee /etc/apt/sources.list.d/spotify.list
  apt-get update
  DEBIAN_FRONTEND=noninteractive apt-get install --yes spotify-client
fi

if [ "$virtual_machine" -eq 1 ]
then
  ./docker_ce.sh
  ./virtualbox.sh
fi

# dot files TODO
