#!/bin/sh

tmux -2 new-session -d -s $USER
tmux new-window -t $USER:0
tmux split-window -h
tmux select-pane -t 0
tmux send-keys 'tail -f ./fedora_log' C-m
tmux split-window -v
tmux send-keys 'tail -f ./ubuntu_log' C-m
tmux select-pane -t 2
tmux send-keys 'tail -f ./debian_log' C-m
tmux split-window -v
tmux send-keys 'sudo ./docker_ce_test.sh' C-m
tmux attach
